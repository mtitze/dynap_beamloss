# Generate beam loss from double Gaussian distributions

Calculates the beam loss for a given set of double Gaussian distributions as a function of the dynamic aperture. 

## Preparation

Prepare a file with the name

	dgaussians.csv

containing the information on the double gaussians to be considered. The general structure is:
    
    a1 sig1 a2 sig2
    
for example
    
    0.800	1.000	0.200	2.400
    0.800	1.000	0.200	2.500
    0.800	1.000	0.200	2.600
    0.800	1.000	0.200	2.700
    0.800	1.000	0.200	2.800


## Execution

Run the getloss.exe script the target directory name as an argument

    >> ./getloss.exe output

In the example, the loss distributions for the different DA values will be written into the directory "output".

## Result

The result will be a directory containing loss files for each DA in steps of 0.1. DA values are considered up to values associated with beam losses below 1e-17. 